package br.com.web.runners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;


@RunWith(Cucumber.class)
@CucumberOptions(
		features = "./features",
		glue = {"br.com.web.steps", "br.com.web.util"},
		snippets = SnippetType.CAMELCASE,
		monochrome = true,
		dryRun = false,
		plugin = {"pretty"}
		)

public class Runner {
	
}