package br.com.web.pages;

import static br.com.web.core.DriverFactory.getDriver;
import static br.com.web.util.Parametros.senhaCadastrada;
import static br.com.web.util.Parametros.usuarioCadastrado;

import org.openqa.selenium.By;

import br.com.web.core.BasePage;

public class LogarPage extends BasePage {
	
	// Elementos Web
	By campoUsuario = By.name("username");
	By campoSenha = By.name("password");
	By botaoEntre = By.cssSelector("button[class*='white--text v-btn v-btn--block v-btn--is-elevated v-btn--has-bg theme--light elevation-5 v-size--x-large purple darken-1']");
	By botaoDashboard = By.cssSelector("button[class*='v-app-bar__nav-icon v-btn v-btn--flat v-btn--icon v-btn--round theme--dark v-size--default']");
	
	public void acessarSite() throws Exception {
		getDriver().get("https://openbanking.smartfy-wrk.smartfylabs.com/");
	}

	public void preencherDadosDeLogin() throws Exception {
		escrever(campoUsuario, usuarioCadastrado);
		escrever(campoSenha, senhaCadastrada);
		clicar(botaoEntre);
	}

	public void validarLogin() {
		clicar2(botaoDashboard);
		clicar(botaoDashboard);
		clicar(botaoDashboard);
		clicar(botaoDashboard); 
	}
}