package br.com.web.steps;

import br.com.web.pages.LogarPage;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.cucumber.java.pt.Dado;




public class LogarSteps {

	private LogarPage page = new LogarPage();

	@Dado("^que um usuario acesse a plataforma smartfy$")
	public void queUmUsuarioAcesseAPlataformaSmartfy() throws Throwable {
		page.acessarSite();
		
	}	
	
	@Quando("^realizar o login de um usuario cadastrado$")
	public void realizarOLoginDeUmUsuarioCadastrado() throws Throwable {
		page.preencherDadosDeLogin();
		
	}

	@Entao("^sera exibida a pagina logada$")
	public void seraExibidaAPaginaLogada() throws Throwable {
		page.validarLogin();
		
	}
}	