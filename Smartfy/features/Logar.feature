#language: pt
Funcionalidade: Logar

	@logar
  Cenario: efetuar login
    Dado que um usuario acesse a plataforma smartfy
    Quando realizar o login de um usuario cadastrado
    Entao sera exibida a pagina logada
